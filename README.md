


# Luiza Labs - Back-end

[![N|Solid](https://workablehr.s3.amazonaws.com/uploads/account/logo/178168/small_luizalabs.png)](https://nodesource.com/products/nsolid)

# New Features!

This project contains terminals that add and list nearby institutions, which can be public or private

##### Endpoints

 - Add institution:
    path: /api/institution
    http method: POST
    Content: Add a new institution
    payload:
    
  

>       {
>           "coordinateX": 41,
>           "coordinateY": 58,
>           "name": "Outback",
>           "closeHour": "00:30:00",
>           "openHour": "09:00:00",
>           "isPublic": false
>         }

        
Obs: For public institutions in't allow add closeHour and openHour

 - Edit institution:
    path: /api/institution
    http method: PUT
    Content: Edit the institution
    payload:
    
  

>        {
>           "coordinateX": 60,
>           "coordinateY": 55,
>           "name": "Mc Donalds",
>           "closeHour": "00:30:00",
>           "openHour": "09:00:00",
>           "isPublic": false
>         }

    

 - List institution:
    path: /api/institution
    http method: GET
    Content: List all institutions register
    payload: none

 - Near institution:
    path: /api/institution/near
    Content: Return all near institution in accord of payload
    payload:
    
  

>       {
>           "distance": 50,
>           "coordinateX": 20,
>           "coordinateY": 10,
>           "hour": "08:51:00"
>         }

    

#### Postman
- [Publish postman](https://documenter.getpostman.com/view/1662721/SzS4R75Z?version=latest)

#### Swagger
This options is valid only after run application
- [Swagger](http://localhost:8080/swagger-ui.html)

### Requirements

For building and running the application you need:

- [Eclipse](https://www.eclipse.org/) ou [Intellij](https://www.jetbrains.com/idea/)
- [Lombok](https://www.baeldung.com/lombok-ide)
- [Mysql](https://medium.com/@leandroembu/como-instalar-o-mysql-no-ubuntu-18-04-2ef208a728fa)
- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally

It is necessary import the application like `Existing Maven Projects`

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.luizalabs.DigitalMaps` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

 ```
     mvn spring-boot:run
  ```

### Step-by-step to run in Intellij

After open the file:
- Install Lombok
   - Go to File > Settings > Plugins
   - Click on Browse repositories...
   - Search for Lombok Plugin
   - Click on Install plugin
   - Restart IntelliJ IDEA

- Mysql
    - Go to application.properties
    - Set username in `spring.datasource.username=`
    - Set password in `spring.datasource.password=`

- JDK8
    - Go to "File"
    - "Project Structure "
    - Then select "Project" in "Project Settings"
    - In "Project SDK" select 1.8 or add

- Now is ready, run ` mvn spring-boot:run` or:
    - Go to `DigitalMapsApplication` in package  `com.luizalabs.DigitalMaps` and with right button select `Rum DigitalMapsApplication.main()`
