package com.luizalabs.DigitalMaps.application.controller;

import com.luizalabs.DigitalMaps.domain.model.Institution;
import com.luizalabs.DigitalMaps.domain.payload.NearInstitution;
import com.luizalabs.DigitalMaps.domain.request.InstitutionRequest;
import com.luizalabs.DigitalMaps.domain.request.ProximityRequest;
import com.luizalabs.DigitalMaps.domain.service.InstitutionService;
import com.luizalabs.DigitalMaps.infrastructure.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/api/institution")
public class InstitutionController {

    @Autowired
    private InstitutionService institutionService;

    @PostMapping
    public ResponseEntity<Institution> institution(@Valid @RequestBody InstitutionRequest institutionRequest) throws BadRequestException {
        return ResponseEntity.ok(institutionService.saveInstitution(institutionRequest));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Institution> institution(@RequestBody InstitutionRequest institutionRequest, @PathVariable Long id) throws BadRequestException {
        return ResponseEntity.ok(institutionService.editInstitution(id, institutionRequest));
    }

    @GetMapping
    public ResponseEntity<List<Institution>> institution() {
        return ResponseEntity.ok(institutionService.getAllInstitutions());
    }

    @GetMapping("/near")
    public ResponseEntity<List<NearInstitution>> institutionByProximity(@Valid @RequestBody ProximityRequest proximityRequest) {
        return ResponseEntity.ok(institutionService.findNearbyInstitutions(proximityRequest));
    }
}
