package com.luizalabs.DigitalMaps.domain.payload;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.luizalabs.DigitalMaps.domain.model.Institution;
import com.luizalabs.DigitalMaps.domain.request.ProximityRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.beans.BeanUtils;

import java.time.LocalTime;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NearInstitution {

    private String name;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    @ApiModelProperty(dataType = "java.lang.String", example = "19:00:0")
    private LocalTime openHour;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    @ApiModelProperty(dataType = "java.lang.String", example = "19:00:0")
    private LocalTime closeHour;

    public Boolean isOpen;

    public static NearInstitution convertToNearInstitution(Institution institution){
        NearInstitution nearInstitution = new NearInstitution();
        BeanUtils.copyProperties(institution, nearInstitution);
        return  nearInstitution;
    }

    public NearInstitution mapOperation(ProximityRequest proximityRequest){
        LocalTime hourToCheck = Objects.nonNull(proximityRequest.getHour()) ? proximityRequest.getHour() : LocalTime.now();
        if (Objects.isNull(this.openHour)) {
            this.isOpen = true;
        } else {
            if(this.closeHour.isBefore(this.openHour)) {
                this.isOpen = this.openHour.isBefore(hourToCheck);
            } else {
                this.isOpen = this.openHour.isBefore(hourToCheck) && this.closeHour.isAfter(hourToCheck);
            }
        }
        return this;
    }


}
