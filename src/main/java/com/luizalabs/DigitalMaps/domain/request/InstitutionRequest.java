package com.luizalabs.DigitalMaps.domain.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.luizalabs.DigitalMaps.infrastructure.annotation.HoursOperation;
import com.luizalabs.DigitalMaps.infrastructure.annotation.NullOrNotBlank;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@HoursOperation(closed = "closeHour", opened ="openHour", isPublic = "isPublic")
public class InstitutionRequest {

    @NullOrNotBlank(message = "Nome do estabelimento não pode ser vazio")
    @ApiModelProperty(value = "Nome do estabelimento valido", example = "Restaurante")
    private String name;

    @DecimalMin(value = "0", message = "Coordenada X invalida")
    @NotNull(message = "Valor de coordenada Y não informada")
    @ApiModelProperty(value = "Coordenada X valido", example = "1")
    private Long coordinateX;

    @DecimalMin(value = "0", message = "Coordenada Y invalida")
    @NotNull(message = "Valor de coordenada Y não informada")
    @ApiModelProperty(value = "Coordenada Y valida", example = "1")
    private Long coordinateY;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    @ApiModelProperty(dataType = "java.lang.String", example = "19:00:0")
    private LocalTime openHour;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    @ApiModelProperty(dataType = "java.lang.String", example = "19:00:0")
    private LocalTime closeHour;

    private Boolean isPublic;

}
