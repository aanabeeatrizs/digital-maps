package com.luizalabs.DigitalMaps.domain.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProximityRequest {

    @DecimalMin(value = "0", message = "Coordenada Y invalida")
    @NotNull(message = "Coordenada Y não informada")
    @ApiModelProperty(value = "Coordenada Y", example = "1")
    private Long coordinateX;

    @DecimalMin(value = "0", message = "Coordenada X invalida")
    @NotNull(message = "Coordenada X não informada")
    @ApiModelProperty(value = "Coordenada Y", example = "1")
    private Long coordinateY;

    @DecimalMin(value = "0", message = "Distancia invalida")
    @NotNull(message = "Distancia não informada")
    @ApiModelProperty(value = "Distancia", example = "5")
    private Long distance;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    @ApiModelProperty(dataType = "java.lang.String", example = "19:00:0")
    private LocalTime hour;
}
