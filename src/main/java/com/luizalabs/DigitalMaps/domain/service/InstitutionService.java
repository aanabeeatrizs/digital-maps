package com.luizalabs.DigitalMaps.domain.service;

import com.luizalabs.DigitalMaps.domain.model.Institution;
import com.luizalabs.DigitalMaps.domain.payload.NearInstitution;
import com.luizalabs.DigitalMaps.domain.request.InstitutionRequest;
import com.luizalabs.DigitalMaps.domain.request.ProximityRequest;
import com.luizalabs.DigitalMaps.infrastructure.exception.BadRequestException;
import com.luizalabs.DigitalMaps.infrastructure.repository.InstitutionRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class InstitutionService {

    @Autowired
    private InstitutionRepository institutionRepository;

    public Institution saveInstitution(InstitutionRequest institutionRequest) throws BadRequestException {
        Institution institution = new Institution();
        BeanUtils.copyProperties(institutionRequest, institution);
        if (institutionRepository.findFirstByCoordinate(institution.getCoordinateY(), institution.getCoordinateX()).isPresent()){
            throw new BadRequestException("Coordenadas já cadastrada");
        }
        return institutionRepository.save(institution);
    }

    public Institution editInstitution(Long id, InstitutionRequest institutionRequest) throws BadRequestException {
        Institution institution = institutionRepository.findById(id).orElseThrow(() -> new BadRequestException("Instituição não encontrada"));
        if (institutionRepository.findFirstByCoordinate(institutionRequest.getCoordinateY(), institutionRequest.getCoordinateX()).isPresent()){
            throw new BadRequestException("Coordenadas já cadastrada");
        }
        BeanUtils.copyProperties(institutionRequest, institution);
        return institutionRepository.save(institution);
    }

    public List<Institution> getAllInstitutions(){
        return institutionRepository.findAll();
    }

    public List<NearInstitution> findNearbyInstitutions(ProximityRequest proximityRequest) {
        Optional<List<Institution>> nearInstitutions = institutionRepository.findByCoordinateNear(proximityRequest.getCoordinateY(), proximityRequest.getCoordinateX(), proximityRequest.getDistance());
        if(!nearInstitutions.isPresent()){
            return Collections.emptyList();
        }

        return nearInstitutions.get().stream().map(NearInstitution::convertToNearInstitution).map(nearInstitution -> nearInstitution.mapOperation(proximityRequest)).collect(Collectors.toList());
    }
}
