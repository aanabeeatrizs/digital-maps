package com.luizalabs.DigitalMaps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigitalMapsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigitalMapsApplication.class, args);
	}

}
