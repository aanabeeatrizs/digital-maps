package com.luizalabs.DigitalMaps.infrastructure.annotation.validator;

import com.luizalabs.DigitalMaps.domain.request.InstitutionRequest;
import com.luizalabs.DigitalMaps.infrastructure.annotation.HoursOperation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;
import java.time.LocalTime;
import java.util.Objects;

public class HoursOperationValidator implements ConstraintValidator<HoursOperation, InstitutionRequest> {

    private String opened;

    private String closed;

    private String isPublic;

    @Override
    public void initialize(HoursOperation parameters) {
        this.opened = parameters.opened();
        this.closed = parameters.closed();
        this.isPublic = parameters.isPublic();
    }

    @Override
    public boolean isValid(InstitutionRequest institutionRequest, ConstraintValidatorContext constraintValidatorContext) {
        try {
            LocalTime openHour = (LocalTime) getFieldValue(institutionRequest, this.opened);
            LocalTime closeHour = (LocalTime) getFieldValue(institutionRequest, this.closed);
            Boolean isPublic = (Boolean) getFieldValue(institutionRequest, this.isPublic);

            if(Objects.isNull(isPublic) || !isPublic){
                if(Objects.isNull(openHour)){
                    setMessage("Horário de abertura não pode ser nulo", constraintValidatorContext);
                    return false;
                } else if (Objects.isNull(closeHour)) {
                    setMessage("Horário de fechamento não pode ser nulo", constraintValidatorContext);
                    return false;
                }
            } else {
                if(Objects.nonNull(openHour) || Objects.nonNull(closeHour)){
                    setMessage("Intituição publica não deve ter horário de funcionamento.", constraintValidatorContext);
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private Object getFieldValue(Object object, String fieldName) throws Exception {
        Class<?> clazz = object.getClass();
        Field passwordField = clazz.getDeclaredField(fieldName);
        passwordField.setAccessible(true);
        return passwordField.get(object);
    }

    private void setMessage(String message, ConstraintValidatorContext constraintValidatorContext) {
        //disable existing violation message
       constraintValidatorContext.disableDefaultConstraintViolation();
       //build new violation message and add it
       constraintValidatorContext.buildConstraintViolationWithTemplate(message).addConstraintViolation();
    }
}
