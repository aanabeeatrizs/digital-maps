package com.luizalabs.DigitalMaps.infrastructure.annotation;

import com.luizalabs.DigitalMaps.infrastructure.annotation.validator.HoursOperationValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ElementType.TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = HoursOperationValidator.class)
public @interface HoursOperation {

    String message() default "{javax.validation.constraints.Pattern.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String opened();

    String closed();

    String isPublic();


}