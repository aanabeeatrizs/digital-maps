package com.luizalabs.DigitalMaps.infrastructure.repository;

import com.luizalabs.DigitalMaps.domain.model.Institution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InstitutionRepository extends JpaRepository<Institution, Long> {

    @Query(value="SELECT * FROM institution i WHERE i.coordinateY = :coordinateY AND i.coordinateX = :coordinateX limit 1", nativeQuery = true)
    Optional<Institution> findFirstByCoordinate(@Param("coordinateY") Long coordinateY, @Param("coordinateX") Long coordinateX);

    @Query(value = "SELECT i FROM institution i WHERE (:coordinateX <= i.coordinateX + :distance AND :coordinateX >= i.coordinateX - :distance) AND (:coordinateY <= i.coordinateY + :distance AND :coordinateY >= i.coordinateY - :distance)")
    Optional<List<Institution>> findByCoordinateNear(@Param("coordinateY") Long coordinateY, @Param("coordinateX") Long coordinateX, @Param("distance") Long distance);
}
