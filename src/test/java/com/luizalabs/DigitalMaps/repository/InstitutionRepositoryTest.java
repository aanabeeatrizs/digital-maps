package com.luizalabs.DigitalMaps.repository;

import com.luizalabs.DigitalMaps.domain.model.Institution;
import com.luizalabs.DigitalMaps.infrastructure.repository.InstitutionRepository;
import com.luizalabs.DigitalMaps.utils.UtilTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class InstitutionRepositoryTest extends UtilTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private InstitutionRepository institutionRepository;

    @Test
    public void whenFindFirstByCoordinate_thenReturnInstitution() {

        // when
        Optional<Institution> found = institutionRepository.findFirstByCoordinate(privateInstitutionToSave.getCoordinateY(), privateInstitutionToSave.getCoordinateX());

        // then
        assertTrue(found.isPresent());
    }


    @Test
    public void whenFindByCoordinate_thenReturnInstitution() {

        // when
        Optional<List<Institution>> nears = institutionRepository.findByCoordinateNear(10L, 20L, 10L);

        // then
        assertTrue(nears.isPresent() && nears.get().size() == 2);
    }

    @Test
    public void whenFindByCoordinate_thenReturnNoneInstitution() {

        // when
        Optional<List<Institution>> nears = institutionRepository.findByCoordinateNear(100L, 200L, 10L);

        // then
        assertTrue(!nears.isPresent());
    }
}
