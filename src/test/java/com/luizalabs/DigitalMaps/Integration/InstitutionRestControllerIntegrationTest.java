package com.luizalabs.DigitalMaps.Integration;

import com.luizalabs.DigitalMaps.utils.UtilTest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Objects;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest()
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class InstitutionRestControllerIntegrationTest extends UtilTest {

    @Autowired
    private MockMvc mvc;

    // Create institution
    @Test
    public void givenInstitution_whenGetInstitution_thenStatus400HourOpenedNull()
            throws Exception {

        Exception exception = mvc.perform(post("/api/institution")
                .content(getJson("/json/integration/new-institution-open-null.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException();

        Assert.assertTrue(Objects.nonNull(exception) && exception.getMessage().contains("Horário de abertura não pode ser nulo"));
    }

    @Test
    public void givenInstitution_whenGetInstitution_thenStatus400WithHourCloseNull()
            throws Exception {

        Exception exception = mvc.perform(post("/api/institution")
                .content(getJson("/json/integration/new-institution-close-null.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException();

        Assert.assertTrue(Objects.nonNull(exception) && exception.getMessage().contains("Horário de fechamento não pode ser nulo"));

    }

    @Test
    public void givenInstitution_whenGetInstitution_thenStatus400AndPublicWithHour()
            throws Exception {

        Exception exception =  mvc.perform(post("/api/institution")
                .content(getJson("/json/integration/new-public-institution-with-hour.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException();

        Assert.assertTrue(Objects.nonNull(exception) && exception.getMessage().contains("Intituição publica não deve ter horário de funcionamento."));
    }

    @Test
    public void givenInstitution_whenGetInstitution_thenStatus200AndPrivate()
            throws Exception {

        mvc.perform(post("/api/institution")
                .content(getJson("/json/integration/new-private-institution.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(getJson("/json/integration/new-private-institution-saved.json")))
                .andExpect(status().isOk());
    }

    @Test
    public void givenInstitution_whenGetInstitution_thenStatus200AndPublic()
            throws Exception {

        mvc.perform(post("/api/institution")
                .content(getJson("/json/integration/new-public-institution.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(getJson("/json/integration/new-public-institution-saved.json")))
                .andExpect(status().isOk());
    }

    @Test
    public void givenInstitution_whenGetInstitution_thenStatus400WithInvalidCoordinate()
            throws Exception {

        Exception exception = mvc.perform(post("/api/institution")
                .content(getJson("/json/integration/new-private-invalid-institution.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException();

        Assert.assertTrue(Objects.nonNull(exception) && exception.getMessage().contains("Coordenadas já cadastrada"));
    }

    @Test
    public void editInstitution_whenGetInstitution_thenStatus200()
            throws Exception {


        mvc.perform(put("/api/institution/4")
                .content(getJson("/json/integration/edit-institution.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(getJson("/json/integration/edit-institution-saved.json")))
                .andExpect(status().isOk());
    }

    @Test
    public void ediInstitution_whenNotFoundInstitution_thenStatus400BadRequestl()
            throws Exception {

        Exception exception = mvc.perform(put("/api/institution/10")
                .content(getJson("/json/integration/edit-institution.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException();

        Assert.assertTrue(Objects.nonNull(exception) && exception.getMessage().contains("Instituição não encontrada"));

    }


    @Test
    public void editInstitution_whenFoundSameCoordinate_thenStatus400BadRequest()
            throws Exception {

        Exception exception = mvc.perform(put("/api/institution/4")
                .content(getJson("/json/integration/edit-institution-same-coordinate.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException();

        Assert.assertTrue(Objects.nonNull(exception) && exception.getMessage().contains("Coordenadas já cadastrada"));
    }

    //Find all institution
    @Test
    public void search_whenGetInstitution_thenStatus200WithInstitutions() throws  Exception{
        mvc.perform(get("/api/institution")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(getJson("/json/integration/all-institutions.json")));
    }

    //Find all near institution
    @Test
    public void searchNear_whenGetInstitution_thenStatus200WithInstitutionsOpenClose() throws  Exception{
        mvc.perform(get("/api/institution/near")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getJson("/json/integration/proximity.json")))
                .andExpect(status().isOk())
                .andExpect(content().json(getJson("/json/integration/all-near-institutions-open-closed.json")));
    }

    @Test
    public void searchNear_whenGetInstitution_thenStatus200WithInstitutionsOpen() throws  Exception{
        mvc.perform(get("/api/institution/near")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getJson("/json/integration/proximity-open.json")))
                .andExpect(status().isOk())
                .andExpect(content().json(getJson("/json/integration/all-near-institutions-open.json")));
    }

    @Test
    public void searchNear_whenGetInstitution_thenStatus200WithNoInstitutions() throws  Exception{
        mvc.perform(get("/api/institution/near")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getJson("/json/integration/no-proximity.json")))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

}
