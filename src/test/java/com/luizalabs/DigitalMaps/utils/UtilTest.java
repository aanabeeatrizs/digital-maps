package com.luizalabs.DigitalMaps.utils;

import com.luizalabs.DigitalMaps.domain.model.Institution;
import com.luizalabs.DigitalMaps.domain.payload.NearInstitution;
import com.luizalabs.DigitalMaps.domain.request.InstitutionRequest;
import com.luizalabs.DigitalMaps.domain.request.ProximityRequest;
import io.micrometer.core.instrument.util.IOUtils;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

public class UtilTest {

    // Used in InstitutionServiceTest and InstitutionControllerTest
    protected Institution institutionSaved = Institution.builder().id(1L).name("Mouse Cake").name("Mouse Cake").coordinateX(27L).coordinateY(12L).closeHour(LocalTime.of(18,0)).openHour(LocalTime.of(12, 0)).isPublic(false).build();

    private Institution publicInstitutionSaved = Institution.builder().id(2L).name("Praça").coordinateX(15L).coordinateY(12L).openHour(null).closeHour(null).isPublic(true).build();

    protected List<Institution> allInstitutiton = Arrays.asList(institutionSaved, publicInstitutionSaved);


    // Used in InstitutionServiceTest
    protected Institution privateInstitutionToSave = Institution.builder().name("Mouse Cake").coordinateX(27L).coordinateY(12L).closeHour(LocalTime.of(18,0)).openHour(LocalTime.of(12, 0)).isPublic(false).build();

    protected Institution privateInstitutionToSaveOtherDay = Institution.builder().name("Outback").coordinateX(41L).coordinateY(32L).closeHour(LocalTime.of(0,30)).openHour(LocalTime.of(18, 0)).isPublic(false).build();

    protected InstitutionRequest institutionRequest = InstitutionRequest.builder().name("Mouse Cake").coordinateX(27L).coordinateY(12L).isPublic(true).build();

    protected Institution publicInstitutionToSave = Institution.builder().name("Praça").coordinateX(15L).coordinateY(12L).isPublic(true).build();

    protected ProximityRequest proximityRequest = ProximityRequest.builder().coordinateX(20L).coordinateY(10L).distance(10L).hour(LocalTime.of(19,0)).build();


    // Used in InstitutionControllerTest
    private NearInstitution nearInstitutionClosed = NearInstitution.builder().name("Mouse Cake").isOpen(false).openHour(LocalTime.of(12, 0)).closeHour(LocalTime.of(18,0)).build();

    private NearInstitution nearInstitutionOpen = NearInstitution.builder().name("Praça").openHour(null).closeHour(null).isOpen(true).build();

    protected List<NearInstitution> allNearIntitutiton = Arrays.asList(nearInstitutionClosed, nearInstitutionOpen);


    protected String getJson(String fileName) {
        return IOUtils.toString(
                this.getClass().getResourceAsStream(fileName)
        );
    }

}
