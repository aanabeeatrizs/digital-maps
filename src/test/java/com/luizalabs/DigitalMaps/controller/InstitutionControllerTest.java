package com.luizalabs.DigitalMaps.controller;

import com.luizalabs.DigitalMaps.application.controller.InstitutionController;
import com.luizalabs.DigitalMaps.domain.service.InstitutionService;
import com.luizalabs.DigitalMaps.infrastructure.exception.BadRequestException;
import com.luizalabs.DigitalMaps.utils.UtilTest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Objects;

import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(InstitutionController.class)
public class InstitutionControllerTest extends UtilTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private InstitutionService institutionService;

    @Test
    public void givenInstitution_whenGetInstitution_thenReturnNewInstitution()
            throws Exception {

        when(institutionService.saveInstitution(any())).thenReturn(super.institutionSaved);

        mvc.perform(post("/api/institution")
                .content(getJson("/json/save-institution.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(getJson("/json/saved-institution.json")))
                .andExpect(status().isOk());
    }

    @Test
    public void givenInstitution_whenFoundSameCoordinate_thenThrowBadRequest()
            throws Exception {

        when(institutionService.saveInstitution(any())).thenThrow(new BadRequestException("Coordenadas já cadastrada"));

        Exception exception = mvc.perform(post("/api/institution")
                .content(getJson("/json/save-institution.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException();

        Assert.assertTrue(Objects.nonNull(exception) && exception.getMessage().contains("Coordenadas já cadastrada"));
    }

    @Test
    public void editInstitution_whenGetInstitution_thenReturnInstitution()
            throws Exception {

        when(institutionService.editInstitution(any(), any())).thenReturn(super.institutionSaved);

        mvc.perform(put("/api/institution/1")
                .content(getJson("/json/save-institution.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(getJson("/json/saved-institution.json")))
                .andExpect(status().isOk());
    }

    @Test
    public void editInstitution_whenNotFoundInstitution_thenThrowBadRequest()
            throws Exception {

        when(institutionService.editInstitution(any(), any())).thenThrow(new BadRequestException("Instituição não encontrada"));

        Exception exception = mvc.perform(put("/api/institution/1")
                .content(getJson("/json/save-institution.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException();

        Assert.assertTrue(Objects.nonNull(exception) && exception.getMessage().contains("Instituição não encontrada"));
    }

    @Test
    public void editInstitution_whenFoundSameCoordinate_thenReturnBadRequest()
            throws Exception {

        when(institutionService.editInstitution(any(), any())).thenThrow(new BadRequestException("Coordenadas já cadastrada"));

        Exception exception = mvc.perform(put("/api/institution/1")
                .content(getJson("/json/save-institution.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException();

        Assert.assertTrue(Objects.nonNull(exception) && exception.getMessage().contains("Coordenadas já cadastrada"));
    }

    @Test
    public void searchInstitution_whenGetInstitutions_thenReturnAllInstitution()
            throws Exception {

        when(institutionService.getAllInstitutions()).thenReturn(super.allInstitutiton);

        mvc.perform(get("/api/institution")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(getJson("/json/all-institution.json")))
                .andExpect(status().isOk());
    }

    @Test
    public void searchInstitution_whenNotFoundNoInstitution_thenReturnNoInstitution()
            throws Exception {

        given(institutionService.getAllInstitutions()).willReturn(Collections.emptyList());

        mvc.perform(get("/api/institution")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    @Test
    public void searchNearInstitution_whenGetInstitutions_thenReturnNearInstitutions()
            throws Exception {

        given(institutionService.findNearbyInstitutions(any())).willReturn(super.allNearIntitutiton);

        mvc.perform(get("/api/institution/near")
                .content(getJson("/json/proximity.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(getJson("/json/all-near-institutions.json")))
                .andExpect(status().isOk());
    }

    @Test
    public void searchNearInstitution_whenNotFoundInstitution_thenReturnNoNearInstitutions()
            throws Exception {

        given(institutionService.findNearbyInstitutions(any())).willReturn(Collections.emptyList());

        mvc.perform(get("/api/institution/near")
                .content(getJson("/json/proximity.json"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("[]"))
                .andExpect(status().isOk());
    }
}
