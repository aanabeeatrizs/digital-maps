package com.luizalabs.DigitalMaps.service;

import com.luizalabs.DigitalMaps.domain.model.Institution;
import com.luizalabs.DigitalMaps.domain.payload.NearInstitution;
import com.luizalabs.DigitalMaps.domain.service.InstitutionService;
import com.luizalabs.DigitalMaps.infrastructure.exception.BadRequestException;
import com.luizalabs.DigitalMaps.infrastructure.repository.InstitutionRepository;
import com.luizalabs.DigitalMaps.utils.UtilTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalTime;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class InstitutionServiceTest extends UtilTest {

    @TestConfiguration
    static class InstitutionServiceImplTestContextConfiguration {

        @Bean
        public InstitutionService institutionService() {
            return new InstitutionService();
        }
    }

    @Autowired
    private InstitutionService institutionService;

    @MockBean
    private InstitutionRepository institutionRepository;

    @Test
    public void whenValidInstitution_thenInstitutionShouldBeSave() {
        Mockito.when(institutionRepository.findFirstByCoordinate(super.privateInstitutionToSave.getCoordinateY(), super.privateInstitutionToSave.getCoordinateX()))
                .thenReturn(Optional.empty());
        Mockito.when(institutionRepository.save(ArgumentMatchers.any())).thenReturn(super.institutionSaved);
        Institution newInstitution = institutionService.saveInstitution(super.institutionRequest);

        assertThat(newInstitution.getId()).isNotNull();
    }

    @Test(expected = BadRequestException.class)
    public void whenCoordinateInvalid_thenInstitutionShouldNotBeSave() {
        Mockito.when(institutionRepository.findFirstByCoordinate(super.privateInstitutionToSave.getCoordinateY(), super.privateInstitutionToSave.getCoordinateX()))
                .thenReturn(Optional.of(institutionSaved));
        Mockito.when(institutionRepository.save(ArgumentMatchers.any())).thenReturn(super.institutionSaved);
        Institution newInstitution = institutionService.saveInstitution(super.institutionRequest);

        assertThat(newInstitution.getId()).isNull();
    }

    @Test
    public void whenEditInstitution_thenInstitutionShouldBeSave() {
        Mockito.when(institutionRepository.findById(ArgumentMatchers.any()))
                .thenReturn(Optional.of(super.institutionSaved));
        Mockito.when(institutionRepository.findFirstByCoordinate(super.privateInstitutionToSave.getCoordinateY(), super.privateInstitutionToSave.getCoordinateX()))
                .thenReturn(Optional.empty());
        Mockito.when(institutionRepository.save(ArgumentMatchers.any())).thenReturn(super.institutionSaved);
        Institution newInstitution = institutionService.editInstitution(1L, super.institutionRequest);

        assertThat(newInstitution.getId()).isNotNull();
    }

    @Test(expected = BadRequestException.class)
    public void whenEditInstitution_thenInstitutionNotFound() {
        Mockito.when(institutionRepository.findById(ArgumentMatchers.any()))
                .thenReturn(Optional.empty());
        Institution newInstitution = institutionService.editInstitution(1L, super.institutionRequest);

        assertThat(newInstitution.getId()).isNull();
    }

    @Test(expected = BadRequestException.class)
    public void whenEditInstitution_thenInstitutionWithInvalidCoordinate() {
        Mockito.when(institutionRepository.findById(ArgumentMatchers.any()))
                .thenReturn(Optional.of(super.institutionSaved));
        Mockito.when(institutionRepository.findFirstByCoordinate(super.privateInstitutionToSave.getCoordinateY(), super.privateInstitutionToSave.getCoordinateX()))
                .thenReturn(Optional.of(super.institutionSaved));
        Institution newInstitution = institutionService.editInstitution(1L, super.institutionRequest);

        assertThat(newInstitution.getId()).isNull();
    }

    @Test
    public void whenSearchAllInstitution_thenInstitutionsFounds(){
        Mockito.when(institutionRepository.findAll())
                .thenReturn(super.allInstitutiton);
        List<Institution> allInstitutions = institutionService.getAllInstitutions();

        assertThat(allInstitutions.equals(super.allInstitutiton)).isTrue();

    }

    @Test
    public void whenSearchAllInstitution_thenInstitutionsNotFounds(){
        Mockito.when(institutionRepository.findAll())
                .thenReturn(Collections.emptyList());
        List<Institution> allInstitutions = institutionService.getAllInstitutions();

        assertThat(allInstitutions.equals(Collections.emptyList())).isTrue();
    }

    @Test
    public void whenSearchInstitutionProximity_thenInstitutionsFounds(){
        Mockito.when(institutionRepository.findByCoordinateNear(ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong()))
                .thenReturn(Optional.of(Arrays.asList(super.privateInstitutionToSave, super.publicInstitutionToSave)));
        List<NearInstitution> allInstitutions = institutionService.findNearbyInstitutions(super.proximityRequest);

        assertThat(allInstitutions.size() > 1).isTrue();
    }

    @Test
    public void whenSearchInstitutionProximity_thenInstitutionsFoundsClosed(){
        Mockito.when(institutionRepository.findByCoordinateNear(ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong()))
                .thenReturn(Optional.of(Arrays.asList(super.privateInstitutionToSave, super.publicInstitutionToSave)));
        List<NearInstitution> allInstitutions = institutionService.findNearbyInstitutions(super.proximityRequest);

        assertThat(allInstitutions.stream().anyMatch(nearInstitution -> !nearInstitution.isOpen)).isTrue();
    }

    @Test
    public void whenSearchInstitutionProximity_thenInstitutionsFoundsOpen(){
        Mockito.when(institutionRepository.findByCoordinateNear(ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong()))
                .thenReturn(Optional.of(Arrays.asList(super.privateInstitutionToSave, super.publicInstitutionToSave)));
        List<NearInstitution> allInstitutions = institutionService.findNearbyInstitutions(super.proximityRequest);

        assertThat(allInstitutions.stream().anyMatch(nearInstitution -> nearInstitution.isOpen)).isTrue();
    }

    @Test
    public void whenSearchInstitutionProximity_thenInstitutionsFoundsClosedInNextDay(){
        Mockito.when(institutionRepository.findByCoordinateNear(ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong()))
                .thenReturn(Optional.of(Arrays.asList(super.privateInstitutionToSave, super.publicInstitutionToSave, super.privateInstitutionToSaveOtherDay)));
        List<NearInstitution> allInstitutions = institutionService.findNearbyInstitutions(super.proximityRequest);

        assertThat(allInstitutions.stream().anyMatch(nearInstitution -> nearInstitution.isOpen && Objects.nonNull(nearInstitution.getCloseHour()) && nearInstitution.getCloseHour().isAfter(LocalTime.of(0,0)))).isTrue();
    }

    @Test
    public void whenSearchInstitutionProximity_thenInstitutionsNotFounds(){
        Mockito.when(institutionRepository.findByCoordinateNear(ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong()))
                .thenReturn(Optional.of(Collections.emptyList()));
        List<NearInstitution> allInstitutions = institutionService.findNearbyInstitutions(super.proximityRequest);

        assertThat(allInstitutions.equals(Collections.emptyList())).isTrue();
    }


}
